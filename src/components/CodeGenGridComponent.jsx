import React from 'react';
import MyProjectsGridAction from '../action/CodeGenGridAction';
import { Link } from 'react-router-dom';
import MyProjectsStore from '../store/Store';
import Pagination from "react-js-pagination";
import dateFormat from 'dateformat';
import * as $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
// import '../css/commontheme.css'
// import '../css/stylesheet.css'



/*
*   Replace the image files path as needed
*/




export default class MyProjectsGridComponent extends React.Component {
  constructor(props) {
    super(props)
    {
      this.state = {

        countPerPage: 10,
        Project: '',
        Component: '',
        ProjectLead: '',
        Framework: '',
        Service: '',
        Database: '',
        filterData: [[], [], [], []],
        // hidefilter : 0,

        searchValue: '',
        columnOrder: null,
        columnName: null,
        sorthide: '',
        totalItemsCount: 0,
        gridData: [],

      }
    }
  }

  /**
   * GC_PC_01
   * GC_PC_02 call loadGridPage() with the parameter int 1
   * GC_PC_12
   * componentWillMount() catchs the keyword 'loadgrid' then invoke Respective EventListener which invokes loadGrid() 
   * GC_PC_25
   * catchs the keyword 'deleteRecord' then invoke Respective EventListener which invokes arrow function
   * GC_PC_47 
   *  catchs the keyword 'loadFilter' then invoke Respective EventListener which invokes loadFilter() 
  */
  componentDidMount = () => {

    this.loadGridPage(1);

    MyProjectsStore.on('loadgrid', this.loadGrid)

    MyProjectsStore.on('loadFilter', this.loadFilter);


  }

  componentWillUnmount = () => {
    MyProjectsStore.removeListener("loadgrid", this.loadGrid);

    MyProjectsStore.removeListener('loadFilter', this.loadFilter);



  }

  /**
  *GC_PC_03
  *create new json collection searchFilterData with the State variable (filterType1, filterType2, filterType3 and searchValue)
  * GC_PC_04
  * call other function gridLoad() with parameter pageNumber and searchFilter in GridComponentsAction.js using GridAction
  * GC_PC_34
  * change the state variable activePage to pagenumber
  */
  loadGridPage = () => {

    let loadGridPostData = JSON.stringify({
      SearchValue: this.state.searchValue,

      Project: this.state.Project,
      Component: this.state.Component,
      ProjectLead: this.state.ProjectLead,
      Framework: this.state.Framework,
      Service: this.state.Service,
      Database: this.state.Database,


      columnName: this.state.columnName,
      columnOrder: this.state.columnOrder,

      PageNumber: 1,//this.state.pageNumber,
      PageSize: this.state.countPerPage

    })
    console.log(loadGridPostData);
    MyProjectsGridAction.gridLoad(loadGridPostData);

    //  this.setState({
    //     hideFilterPopUp : true
    // })

    // this.state = {
    //     ...this.state,
    //     hideFilterPopUp : true
    // }

    //         this.state = {
    //     ...this.state,
    //     activePage: page
    // }
    // this.setState({
    //     activePage: page
    // })

  }



  handleSearch = (e) => {
    this.setState({
      searchValue: e.target.value
    })
    this.state = { ... this.state, searchValue: e.target.value }
    this.loadGridPage(1)
  }

  /**
* GC_PC_39
* call filterDataLoad() inGridComponentsAction.js usisng the Instance GridAction
* GC_PC_49
* change the state variable hideFilterPopUP to false if it is true or change to true if it is false
*/
  filterClicked = () => {
    console.log(this.state.filterData.length);
    if (this.state.filterData[0].length < 1) {
      MyProjectsGridAction.filterDataLoad();
    }


    // this.setState({
    //     hideFilterPopUp : this.state.hideFilterPopUp == true ? false : true
    // })
    // console.log(this.state.hideFilterPopUp);
  }

  /**
* GC_PC_48
*  Assign store Response to State Variable and Render UI
*/
  loadFilter = () => {
    let Response = MyProjectsStore.Response;
    this.setState({ filterData: Response.ResponseData })
    console.log(Response.ResponseData);
  }


  /**
* GC_PC_50
* render dynamic value to the Filter
*/

  filterDataLoad = (e) => {
    console.log(this.state.filterData);
    // if (e == "ProjectLead") {
    //   return this.state.filterData[0].map((obj, index) => {
    //     return (
    //       <option value={obj.ProjectLeadID}>{obj.ProjectLeadName}</option>
    //     )
    //   })
    // } 
    if (e == "Framework") {
      return this.state.filterData[0].map((obj, index) => {
        if (index == 1 || index == 3) {
          return (
            <option value={obj.FrontendID}>{obj.FrontendName}</option>
          )
        }
      })
    } if (e == "Service") {
      return this.state.filterData[1].map((obj, index) => {
        if (index == 2 ) {
          return (
            <option value={obj.ServiceID}>{obj.ServiceName}</option>
          )
        }

      })
    } if (e == "Database") {
      return this.state.filterData[2].map((obj, index) => {
        if (index == 1 || index == 4) {
          return (
            <option value={obj.DatabaseID}>{obj.DatabaseName}</option>
          )
        }

      })
    }

  }

  /**
* GC_PC_31
* change state variable  (filterType1, filterType2 and filterType3 ) to empty String
*/
  filterCancelClicked = () => {
    
    this.setState({

      Project: "",

      Component: "",

      ProjectLead: "",

      Framework: "",

      Service: "",

      Database: "",

    })
  }

  /**
* GC_PC_28
* update the  state variable ( searchValue, filterType1, filterType2 and filterType3) based on  e.target.name (if - else condition)
*/
  handleFilterInput = async (e) => {

    if (e.target.name == "Project") {
      this.setState({ Project: e.target.value })
    }
    if (e.target.name == "Component") {
      this.setState({ Component: e.target.value })
    }
    if (e.target.name == "ProjectLead") {
      this.setState({ ProjectLead: e.target.value })
    }
    if (e.target.name == "Framework") {
      this.setState({ Framework: e.target.value })
    }
    if (e.target.name == "Service") {
      this.setState({ Service: e.target.value })
    }
    if (e.target.name == "Database") {
      this.setState({ Database: e.target.value })
    }

  }

  sortClicked = (e) => {
    let sortID = e.target.id;
    let sortval = sortID.split('@');
    this.state = {
      ...this.state,
      columnName: sortval[0],
      columnOrder: sortval[1],
      sorthide: sortID
    }

    this.setState({
      columnName: sortval[0],
      columnOrder: sortval[1],
      sorthide: sortID
    })
    this.loadGridPage();
  }


  /**
  *GC_PC_13
  *loadGrid() Assign store Response to State Variable and Render UI
  */
  loadGrid = () => {
    let Response = MyProjectsStore.Response;
    this.setState({ gridData: Response.ResponseData[0] })

    this.setState({ totalItemsCount: Response.ResponseData[1][0].TotalCount })
  }

  handleLoadMore = () => {
    this.state.countPerPage = this.state.countPerPage + 5;
    this.loadGridPage()
  }
  /**
  *render the render  table body  content
  */
  gridBodyData = () => {
    
    // let download = this.CodeGenID
    debugger
    return this.state.gridData.map((obj, index) => {

      return (
        <tr className="trclass" >
          <td className="spacing">{obj.Project}</td>
          <td className="spacing ">{obj.Module}</td>
          <td className="spacing ">{obj.ComponentName}</td>
          <td className="spacing ">{obj.ComponentType}</td>
          <td className="spacing ">{obj.FrontendName}</td>
          <td className="spacing ">{obj.ServiceName}</td>
          <td className="spacing ">{obj.DatabaseName}</td>
          <td className="spacing ">{obj.ProjectLeadMailID}</td>
          <td className="spacing text-center ">
            {obj.CodeStatus == 1 ? <span class="dot-done"></span> : <span class="dot-draft"></span>}
          </td>

          <td className="text-center icon-spacing">
            {obj.CodeStatus == 1 ? <a href={`https://avaeuscdegennpstgacc.blob.core.windows.net/ava-cdegen-zip-np-con/${obj.GeneratedCodePath}`}  ><img className="icons" src="images/Download.svg" id={obj.CodeGenID} /></a> :
              <a href={`/form?colName=CodeGenID&reqData=${obj.CodeGenID}&pMode=edit`}>
                <img className="icons" src="images/Upload.svg" id={obj.CodeGenID} />
              </a>
            }
          </td>
        </tr>
      )
    })
  }

  download = e => {
    console.log(e.target.href);
    fetch(e.target.href, {
      method: "GET",
      headers: {}
    })
      .then(response => {
        response.arrayBuffer().then(function (buffer) {
          const url = window.URL.createObjectURL(new Blob([buffer]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", "image.png"); //or any other extension
          document.body.appendChild(link);
          link.click();
        });
      })
      .catch(err => {
        console.log(err);
      });
  };


  filterdiv=(event)=>
  {
    event.stopPropagation();
  }


  render() {
    return (
      <div>



        <link rel="stylesheet" href="css/commontheme.css" />
        <link rel="stylesheet" href="css/stylesheet1.css" />



        {/*Header Starts Here*/}
        <div className="container-fluid CodeGen-Container">
          <div className="row">
            <div className="col-md-12">
              <div className="float-start">
                <h5 className="header float-start mt-2">My Projects</h5>
              </div>
              <div className="float-end">
                <input type="text" className="form-control float-start search-bar" id="exampleFormControlInput1" placeholder="Search" onChange={this.handleSearch} />
                <div className="dropdown float-start" >
                <button type="button" className="btn filter-button btn-primary-audit ml-3 mb-3 float-start" title="Advanced Filter" data-bs-toggle="dropdown" onClick={this.filterClicked} >
                  <img className="icon-width" src="images/filter-icon.svg" alt="filter-icon" />
                </button>

                <div className="dropdown-menu m-0 pb-4 advanced-filter-table"  onClick={(e) => this.filterdiv(e)}>
                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-md-12 pb-4 pt-2 border-0 mb-1">
                        <span className="FIlter-title">Filter</span>
                        <button type="button" className="btn-close filter-close float-end" id="close-filter" onClick={() =>{
                                                                                                              $(".advanced-filter-table").removeClass("show");
                                                                                                              $(".filter-button").removeClass("show");}}>

                        </button>
                      </div>
                      <div className="col-md-12">
                        <div className="row ">
                          <div className="col-md-6 mb-3">
                            <label className="mb-0 filter-label" htmlFor="Project-Name">Project</label>
                            <input type="text" className="form-control" id="Project-Name" placeholder="Enter Project" name="Project" value={this.state.Project} onChange={this.handleFilterInput} id="Project" />
                          </div>
                          <div className="col-md-6 mb-3">
                            <label className="mb-0 filter-label" htmlFor="Component-name">Component</label>
                            <select className="form-select" aria-label="Default select example" id="Component" name="Component" value={this.state.Component} onChange={this.handleFilterInput}>
                              <option value="">Select Component</option>
                              <option value="Form">Form</option>
                              <option value="Grid">Grid</option>
                            </select>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6 mb-3">
                                  <label className="mb-0 filter-label" htmlFor="Lead">Project Lead </label>
                                  <input className="form-control" aria-label="Default select example" id="ProjectLead" placeholder="Enter Lead Email" name="ProjectLead" value={this.state.ProjectLead} onChange={this.handleFilterInput}/>
                                  
                            {/* <label className="mb-0 filter-label" htmlFor="Lead">Project Lead</label>
                            <select className="form-select" aria-label="Default select example" id="ProjectLead" name="ProjectLead" value={this.state.ProjectLead} onChange={this.handleFilterInput}>
                              <option value="">Select Component</option>
                              {this.filterDataLoad("ProjectLead")}
                            </select> */}
                          </div>
                          <div className="col-md-6 mb-3">
                            <label className="mb-0 filter-label" htmlFor="FW">Framework</label>
                            <select className="form-select" aria-label="Default select example" id="Framework" name="Framework" value={this.state.Framework} onChange={this.handleFilterInput}>
                              <option value="">Select Framework</option>
                              {this.filterDataLoad("Framework")}
                            </select>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6 mb-3">
                            <label className="mb-0 filter-label" htmlFor="Serv-type">Service</label>
                            <select className="form-select" aria-label="Default select example" id="Service" name="Service" value={this.state.Service} onChange={this.handleFilterInput}>
                              <option value="">Select Service</option>
                              {this.filterDataLoad("Service")}
                            </select>
                          </div>
                          <div className="col-md-6 mb-5">
                            <label className="mb-0 filter-label" htmlFor="DB-Type">Database</label>
                            <select className="form-select" aria-label="Default select example" id="Database" name="Database" value={this.state.Database} onChange={this.handleFilterInput}>
                              <option value="">Select Service</option>
                              {this.filterDataLoad("Database")}
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="float-end">
                          <a href="#" className="clear-link px-4" onClick={this.filterCancelClicked}>Clear</a>
                          <button className="btn filter-butt  ml-2 " onClick={() => {
                                                                                  $(".advanced-filter-table").removeClass("show");
                                                                                  $(".filter-button").removeClass("show");
                                                                                this.loadGridPage(1)
                                                                                } }>Apply Filter</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
                <a href="/form" className="btn btn-primary float-start Master-button ms-2"><img src="images/plus.svg" className="plus-icon" alt="Help-Icon" /> Generate New Code</a>
              </div>
            </div>
          </div>
          {/*Header Ends Here*/}
          {/*Grid Starts Here*/}
          <div className="row">
            <div className="col-md-12">
              <table className="table">
                <thead>
                  <tr className="border-class header-bg spacing">
                    <th className="border-class fw-bold spacing">Project</th>
                    <th className="border-class spacing">Module</th>
                    <th className="border-class spacing sorting">Component
                      <img className="sorting-up ps-2" src="images/Sort-Down.svg" alt="ComponentName" id="ComponentName@Desc" onClick={e => this.sortClicked(e)} hidden={this.state.sorthide == "ComponentName@Desc" ? true : false} />
                      <img className="sorting-up ps-2" src="images/Sort-Down.svg" alt="ComponentName" id="ComponentName@Asc" onClick={e => this.sortClicked(e)} hidden={this.state.sorthide == "ComponentName@Desc" ? false : true} />
                    </th>
                    <th className="border-class spacing">Type</th>
                    <th className="border-class spacing">Frontend</th>
                    <th className="border-class spacing">Service</th>
                    <th className="border-class spacing">Database</th>
                    <th className="border-class spacing">Project Lead</th>
                    <th className="border-class text-center spacing">Status</th>
                    <th className="border-class text-center spacing">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.totalItemsCount == 0 ? <div >No Records Found</div> : this.gridBodyData()}
                </tbody>
              </table>
            </div>
          </div>
          {/*Grid Ends Starts Here*/}
          {
            this.state.totalItemsCount > this.state.countPerPage ?

              <div className="load-more text-center mb-3 mt-3">
                <button type="button" onClick={this.handleLoadMore} className="btn btn-outline-secondary load-butt">Load More</button>
              </div>
              : ""}
        </div>
      </div>
    );
  }

}